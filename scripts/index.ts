const youSendMsg = () => {
    const sentMsg=createSentMsg(youTextArea.value);
    const receivedMsg=createReceivedMsg(youTextArea.value);
    youTextSection.appendChild(sentMsg);
    userTextSection.appendChild(receivedMsg);
    youTextArea.value = '';
}

const userSendMsg = () => {
    
    const sentMsg=createSentMsg(userTextArea.value);
    const receivedMsg=createReceivedMsg(userTextArea.value);
    youTextSection.appendChild(receivedMsg);
    userTextSection.appendChild(sentMsg);
    userTextArea.value = '';
}

const createSentMsg = (enteredText) => {

    const sentMsg = document.createElement('p');
    sentMsg.innerHTML = enteredText;
    sentMsg.style.textAlign = 'right';
    return sentMsg;
}

const createReceivedMsg = (enteredText) => {
    const receivedMsg = document.createElement('p');
    receivedMsg.innerHTML = enteredText;
    receivedMsg.style.textAlign = 'left';
    return receivedMsg;
}